#!/bin/bash

#############################
#   0. CHECK REQUIREMENTS
#

if [[ -z "$DRUSH" ]] ; then
  DRUSH=drush
fi
export STUBS_DIR=./stubs
export LOCK_DIR=./lockfiles


#############################
#   1. GET PLATFORM ARGUMENT
#

ACCEPTED_PLATFORMS=(Drupal7 Drupal9)
PLATFORM="$1"
TEST_ALL="false"
if [[ "" == "${PLATFORM}" ]] ; then
    TEST_ALL="true"
elif [[ ! "${ACCEPTED_PLATFORMS[*]}" =~ ${PLATFORM} ]] ; then
    echo "[-] Platform '${PLATFORM}' is not recognized: ${ACCEPTED_PLATFORMS[*]}"
    exit 1
fi


#############################
#   2. BUILD PLATFORMS
#

if [[ "true" == "${TEST_ALL}" || "Drupal7" == "${PLATFORM}" ]] ; then
    export PLATFORM="Drupal7"
    echo "[+] Testing $PLATFORM..."
    DRUSH=$DRUSH make "test-$PLATFORM"
    echo "[+] Testing $PLATFORM ended with exit status = '$?'"
fi
if [[ "true" == "${TEST_ALL}" || "Drupal9" == "${PLATFORM}" ]] ; then
    export PLATFORM="Drupal9"
    echo "[+] Testing $PLATFORM..."
    PLATFORM='Drupal9-' make test-core-drupal9-index
    echo "[+] Testing $PLATFORM ended with exit status = '$?'"
fi
