#!/usr/bin/env bash

ACCEPTED_PLATFORMS=(Drupal7 Drupal9)
PLATFORM="$1"
VERSION="$2"

usage()
{
    echo "[-] Usage: ${0} <platform> <version>"
    echo "[-]   <platform> must be in: ${ACCEPTED_PLATFORMS[*]}"
    echo "[-]   OR use 'ALL' to update all platforms to latest version"
}

###
#   0. ARGUMENT VALIDATION

if [[ -z "$PLATFORM" && -z "$VERSION" ]] ; then usage; exit 1; fi
if [[ "ALL" != "$PLATFORM" && -z "$VERSION" ]] ; then usage; exit 1; fi
if [[ "ALL" != "$PLATFORM" && ! "${ACCEPTED_PLATFORMS[*]}" =~ $PLATFORM ]] ; then usage; exit 1; fi

###
#   1. UPDATE INCLUDES


if [[ "ALL" == "$PLATFORM" ]] ; then
    releases=$(./scripts/check_latest_releases.sh)
    for platform in "${ACCEPTED_PLATFORMS[@]}"; do
        version=$(echo "$releases" | jq -r ".[\"${platform}\"].latest")
        echo "[+] Upgrading $platform to $version"
        ./scripts/update_include.sh "$platform" "$version"
    done
elif [[ "Drupal7" == "$PLATFORM" ]] ; then
    # line looks like:
    #   projects[drupal][download][tag] = pressflow-7.91
    sed -e "s/\(projects\[drupal\]\[download\]\[tag\] = pressflow\)-.*$/\1-${VERSION}/" -i includes/7/core.make
elif [[ "Drupal9" == "$PLATFORM" ]] ; then
    # lines look like:
    #   "drupal/core-composer-scaffold": "9.3.19",
    #   "drupal/core-recommended": "9.3.19",
    sed -e "s/\"\(drupal\/core-composer-scaffold\)\": \".*\"/\"\1\": \"${VERSION}\"/" -i includes/9/composer.json
    sed -e "s/\"\(drupal\/core-recommended\)\": \".*\"/\"\1\": \"${VERSION}\"/" -i includes/9/composer.json
fi
