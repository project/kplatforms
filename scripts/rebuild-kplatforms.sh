#!/bin/bash

if [ -z "$DRUSH" ] ; then
  DRUSH=drush
fi

THIS_COMPOSER=composer
C_VERSION=$(composer -V)
C_PATH=$(command -v composer)
if [[ "$C_VERSION" =~ 1[0-9]*\.[0-9]+\.[0-9]+ ]]; then
  THIS_COMPOSER="php -dmemory_limit=8192M $C_PATH"
fi

STUBS_DIR=./stubs
for i in $(ls $STUBS_DIR); do
  echo "[+] Processing $i..."
  PREFIX=$(cut -d "." -f 1 - <<< "$i")
  if [ -z "$PREFIX" ] ; then
    echo -n " no prefix, skipped."
    continue;
  fi
  if [ "$PREFIX" == 'Drupal9' ]; then
    THIS_COMPOSER=$THIS_COMPOSER PLATFORM='Drupal9-' make build-platform-drupal9
    echo "[+] Build for platform $PREFIX finished with exit status: $?"
  else
    DRUSH=$DRUSH PLATFORM=$PREFIX make build-platform
    echo "[+] Build for platform $PREFIX finished with exit status: $?"
  fi
done;
