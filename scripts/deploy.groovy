#!groovy

// TODO: not comfortable with jenkins having access to infra
// so a better solution would be to have an HTTP endpoint on various aegir servers
// Jenkins would hit that endpoint which would cause a process on aegir servers
// to come fetch the build from jenkins and provision themselves
if (0 == 1) {
    stage('Deploy kplatforms') {
        dir("${WORKSPACE}/kplatforms-deployment") {
            checkout([
                $class: "GitSCM",
                branches: [[name: 'master']],
                extensions: [
                    [$class: "CloneOption", depth: 1, noTags: false, reference: "", shallow: false],
              ],
                userRemoteConfigs: [
                    [credentialsId: "github-ssh", url: "ssh://git.koumbit.net/kplatforms-deployment"]
                ]
            ])
            def deployTemplate = readFile 'deploy-template.yml'
        }

        def aegirProvision = null
        def hosts = []
        hosts.each { host ->
            // TODO: don't forget the withEnv stuff to pass variables to the shell
            Integer exitStatus = sh(
                script: """CURRENT_DATE="$DEPLOY_DATE" \
                            PLATFORM="$i" \
                            DEPLOY_USER="$user" \
                            DEPLOY_HOST="$hostname" \
                            DEPLOY_PATH="$basepath" \
                            DEST="$DEST" \
                            BUILD_NUMBER="$BUILD_NUMBER" \
                            DEPLOY_BASENAME="$basename" \
                        make deploy-platform""",
                returnStatus: true
            )
            if (aegirProvision) {
                // for $path:
                //  /var/aegir/platforms + drupal_9.3.13 + 2022.05.19
                //  = /var/aegir/platforms/drupal_9.3.13_2022.05.19
                // for $sanitized_platform_name:
                //  drupal_9.3.13_2022.05.19
                //  = drupal931320220519
                exitStatus = sh(
                    script: """ssh "$user"@"$hostname" \
                        drush --root="$path" \
                        --context_type='platform' \
                        provision-save @platform_"$sanitized_platform_name" """,
                    returnStatus: true
                )
                exitStatus = sh(
                    script: """ssh "$user"@"$hostname" \
                        drush @hostmaster \
                        hosting-import \
                        @platform_"$sanitized_platform_name" """,
                    returnStatus: true
                )
            }
        }
    }
}
