#!/usr/bin/env bash

# docker run --rm -it -v "$PWD":/src kplatforms:latest /src/scripts/rebuild-kplatforms.sh
PREFIX="$1"
STUBS_DIR="stubs"
if [ -z "$DRUSH" ] ; then
  DRUSH=drush
fi
THIS_COMPOSER=composer
C_VERSION=$(composer -V)
C_PATH=$(command -v composer)
if [[ "$C_VERSION" =~ 1[0-9]*\.[0-9]+\.[0-9]+ ]]; then
  THIS_COMPOSER="php -dmemory_limit=8192M $C_PATH"
fi

if [[ ! -f "./${STUBS_DIR}/${PREFIX}.make" ]] ; then
    echo "[-] Could not find ${PREFIX}.make in ${STUBS_DIR}/"
    exit 1
fi

echo "**************************************************"
echo
echo "[+] STARTING KPLATFORM BUILD FOR ${PREFIX}"
echo
echo "**************************************************"

if [ "$PREFIX" == 'Drupal9' ]; then
    THIS_COMPOSER=$THIS_COMPOSER PLATFORM='Drupal9-' make build-platform-drupal9
    echo "[+] Build for platform $PREFIX finished with exit status: $?"
else
    DRUSH=$DRUSH PLATFORM=$PREFIX make build-platform
    echo "[+] Build for platform $PREFIX finished with exit status: $?"
fi
