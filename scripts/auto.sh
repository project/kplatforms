#!/usr/bin/env bash

PLATFORMS=(Drupal7 Drupal9)
REMOVED_MODULES_LOG_FILE="removed_modules.log"

# if podman can't find 'php:7.3-cli', make sure to add 'unqualified-search-registries=["docker.io"]' to /etc/containers/registries.conf
# installing xpath with apt: libxml-xpath-perl
REQUIRED_TOOLS=(podman jq xpath)
MISSING_TOOLS=""


###
#   0. CHECK REQUIREMENTS

for tool in "${REQUIRED_TOOLS[@]}" ; do
    [[ -z "$(command -v "$tool")" ]] && MISSING_TOOLS="$MISSING_TOOLS $tool"
done
if [[ -n "${MISSING_TOOLS}" ]] ; then
    printf "[-] You are missing the following tools: %s\n" "${MISSING_TOOLS}"
    exit 1
fi


###
#   1. UPDATE INCLUDE FILES WITH LATEST RELEASE VERSIONS

releases=$(./scripts/check_latest_releases.sh)
for platform in "${PLATFORMS[@]}"; do
    version=$(echo "$releases" | jq -r ".[\"${platform}\"].latest")
    ./scripts/update_include.sh "$platform" "$version"
done


###
# 2. CREATE EMPTY BUILD DIR

sudo rm -rf build2 ; mv build build2 ; mkdir build


###
# 3. REBUILD KPLATFORMS IMAGE

podman build -t kplatforms .


###
# 4. BUILD NEW KPLATFORMS

for platform in "${PLATFORMS[@]}"; do
    logfile="build-${platform}.log"
    podman run --rm -it -v "$PWD":/src kplatforms:latest /src/scripts/build-kplatforms.sh "$platform" | tee -a "$logfile"
    # CHECK FOR PACKAGES 'removed'
    printf "[+] REMOVED MODULES in %s:\n\n" "${platform}" >> "${REMOVED_MODULES_LOG_FILE}"
    grep 'removed' "$logfile" >> "${REMOVED_MODULES_LOG_FILE}"
done


###
# 5. TEST KPLATFORMS

podman run --rm -it -v "$PWD":/src kplatforms:latest /src/scripts/test-kplatforms.sh


###
# 6. CHECK FOR REMOVED MODULES

printf "**************************************************\n\n"
printf "[+] CHECK FOR ANY REMOVED MODULES\n"
printf "**************************************************\n\n"
cat "${REMOVED_MODULES_LOG_FILE}"


###
# 7. COPY LOCKFILES

cp -t lockfiles ./build/*.lock ./build/*.txt


###
# 8. GENERATE RELEASE NOTES

./scripts/make-releasenotes.sh > release_notes.txt
