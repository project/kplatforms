#!/usr/bin/env bash

#############################
#   0. CHECK REQUIREMENTS
#

# xpath avec apt: libxml-xpath-perl
REQUIRED_TOOLS=(jq xpath)
MISSING_TOOLS=""

for tool in "${REQUIRED_TOOLS[@]}" ; do
    [[ -z "$(command -v "$tool")" ]] && MISSING_TOOLS="$MISSING_TOOLS $tool"
done
if [[ -n "${MISSING_TOOLS}" ]] ; then
    printf "[-] You are missing the following tools: %s\n" "${MISSING_TOOLS}"
    exit 1
fi

# first item is 'start' rather than latest tag for pressflow/7
#D7_NEW_VERSION=$(curl https://api.github.com/repos/pressflow/7/tags 2>/dev/null | jq -r .[1].name | sed -e 's/pressflow-//')
D7_NEW_VERSION="7.103"
D7_CURRENT_VERSION=$(grep 'projects\[drupal\]\[download\]\[tag\]' includes/7/core.make | sed -e 's/.* = pressflow-\(.*\)$/\1/')
D9_NEW_VERSION=$(curl 'https://git.drupalcode.org/project/drupal/-/tags?format=atom&search=^9' 2>/dev/null \
    | xpath -e '//entry/title/text()' 2> /dev/null | sort -rV | grep -v '\(alpha\|beta\)' | head -1)
D9_CURRENT_VERSION=$(jq .require includes/9/composer.json | jq -r '.["drupal/core-recommended"]')

echo "{
    \"Drupal7\": {
        \"latest\": \"${D7_NEW_VERSION}\",
        \"current\": \"${D7_CURRENT_VERSION}\"
    },
    \"Drupal9\": {
        \"latest\": \"${D9_NEW_VERSION}\",
        \"current\": \"${D9_CURRENT_VERSION}\"
    }
}"
