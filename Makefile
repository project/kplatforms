DEST?=build
TESTSERVER?=
WORKSPACE?=.
DRUSH?=drush7
DEPLOY_PATH?=/var/aegir/platforms
DEPLOY_HOST?=
DEPLOY_USER?=aegir
DEPLOY_BASENAME?=
CURRENT_DATE?=`date +'%Y.%m.%d'`
ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
THIS_COMPOSER?=composer

# @TODO : Respect $(WORKSPACE)/$(DEST) ?
clean:
	rm -rf build 

prep-build:
	if [ ! -d "$(WORKSPACE)/$(DEST)" ] ; then mkdir $(WORKSPACE)/$(DEST); fi


docker: Dockerfile
	docker build -t kplatforms .

build-with-docker: prep-build
	docker run -it -v $(ROOT_DIR):/src kplatforms make clean
	docker run -it -v $(ROOT_DIR):/src kplatforms scripts/rebuild-kplatforms.sh

build-with-composer: prep-build
	mkdir -p $(WORKSPACE)/$(DEST)/$(PLATFORM)
	cp $(WORKSPACE)/includes/9/composer.json $(WORKSPACE)/$(DEST)/$(PLATFORM)
	cd $(WORKSPACE)/$(DEST)/$(PLATFORM) && \
	$(THIS_COMPOSER) update --no-ansi --ignore-platform-req='ext-*' --ignore-platform-req='lib-*' --ignore-platform-req=hhvm && \
	$(THIS_COMPOSER) show > ../Drupal9.RELEASE_NOTES.txt && \
	cp composer.lock ../Drupal9.composer.lock

build-lockfile : prep-build $(WORKSPACE)/stubs/$(PLATFORM).make
	$(DRUSH) make --verbose --no-build --lock=$(WORKSPACE)/$(DEST)/$(PLATFORM).lock $(WORKSPACE)/stubs/$(PLATFORM).make
	make diff-lockfile

diff-lockfile : $(WORKSPACE)/$(DEST)/$(PLATFORM).lock $(WORKSPACE)/lockfiles/$(PLATFORM).lock
	$(DRUSH) make-diff $(WORKSPACE)/lockfiles/$(PLATFORM).lock $(WORKSPACE)/$(DEST)/$(PLATFORM).lock --list=0 > $(WORKSPACE)/$(DEST)/$(PLATFORM).RELEASE_NOTES.txt
	cat $(WORKSPACE)/$(DEST)/$(PLATFORM).RELEASE_NOTES.txt

build-platform : build-lockfile $(WORKSPACE)/$(DEST)/$(PLATFORM).lock
	$(DRUSH) make --concurrency=5 $(WORKSPACE)/$(DEST)/$(PLATFORM).lock $(WORKSPACE)/$(DEST)/$(PLATFORM)-$(BUILD_NUMBER)

build-platform-drupal9 : build-with-composer
	echo 'Done!'

build-from-lock: clean $(WORKSPACE)/lockfiles/$(PLATFORM).lock
	$(DRUSH) make --concurrency=5 $(WORKSPACE)/lockfiles/$(PLATFORM).lock $(WORKSPACE)/$(DEST)/$(PLATFORM)-$(BUILD_NUMBER)

deploy-platform:
	rsync -rqz $(WORKSPACE)/$(DEST)/$(PLATFORM)-$(BUILD_NUMBER)/ $(DEPLOY_USER)@$(DEPLOY_HOST):$(DEPLOY_PATH)/$(DEPLOY_BASENAME)_$(CURRENT_DATE)/

test-Drupal7: test-ckeditor test-leaflet test-core-index

test-ckeditor:
	stat -t $(WORKSPACE)/$(DEST)/$(PLATFORM)-$(BUILD_NUMBER)/sites/all/libraries/ckeditor/ckeditor.js

test-leaflet:
	stat -t $(WORKSPACE)/$(DEST)/$(PLATFORM)-$(BUILD_NUMBER)/sites/all/libraries/leaflet/dist/leaflet.js

test-core-index:
	stat -t $(WORKSPACE)/$(DEST)/$(PLATFORM)-$(BUILD_NUMBER)/index.php

test-core-drupal9-index:
	stat -t $(WORKSPACE)/$(DEST)/$(PLATFORM)/web/index.php

