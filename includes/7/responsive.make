;****************************************
; General
;****************************************

; Description
; A drush makefile for "Responsive Design modules"

; drush make API version
api = 2

; Drupal core
core = 7.x

;****************************************
; Core
;****************************************

/projects[] = picture // pinned
projects[] = fitvids
projects[] = adaptive_image
projects[] = responsive_menus
projects[] = modernizr
projects[] = retina_images

;****************************************
; End
;****************************************
