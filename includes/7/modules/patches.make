;****************************************
; General
;****************************************

; Description
; A drush makefile for Drupal modules.

; drush make API version
api = 2

; Drupal core
core = 7.x

;****************************************
; Modules
;****************************************

; patch for backup_migrate to disallow backing up ALL aegir websites
; from a single site
; rdm#42473
; https://www.drupal.org/files/issues/support_multisite_setups-2606288-6.patch
projects[backup_migrate][patch][2606288] = https://git.drupalcode.org/project/kplatforms/-/raw/4.x/includes/7/modules/patches/2606288_backup_migrate_koumbit.patch

; This patch generate an error now that will not allow the building of the kplatform.
; A new version was released on Sept 19 2016: 7.x-1.2
; See https://www.drupal.org/project/entityreference
; https://drupal.org/node/1858402
; People seems to be able to disable a Field module which still has fields, and core seems to call field hooks on disabled modules
; https://drupal.org/node/1459540#comment-6810146
;projects[entityreference][version] = 1.1
;projects[entityreference][patch][1459540] = http://drupal.org/files/entityreference-1459540-47-workaround-fatal-error.patch

; patch wysiwyg for htauth sites
; https://drupal.org/node/1980850
; https://drupal.org/node/1802394#comment-6556656
; Emmanuel 2017-01-13 commenting below, wysiwyg module is at 2.3: https://www.drupal.org/project/wysiwyg
;projects[wysiwyg][version] = 2.2
;projects[wysiwyg][patch][1802394] = http://drupal.org/files/wysiwyg-1802394-4.patch

;projects[views][patch][1685144] = "http://www.drupal.org/files/views-1685144-localization-bug_1.patch"

; patch boxes for wysiwys ckeditor in civicrm compatibility
projects[boxes][patch][2175471] = "https://www.drupal.org/files/issues/boxes_civicrm_wysiwyg_conflict-2175471-2.patch"
projects[paypal_payment][patch][2870870] = https://www.drupal.org/files/issues/install_file_issue.2870870.2.patch

; Mailsystem's update hook 7300 can destroy platform files and other such nonsense
; @see https://www.drupal.org/project/mailsystem/issues/1690078
projects[mailsystem][patch][1690078] = "https://www.drupal.org/files/issues/2019-09-21/mailsystem-update_hook_7300_file_deletion-1697008-19.patch"

;****************************************
; End
;****************************************
