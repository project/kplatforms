;****************************************
; General
;****************************************

; Description
; A drush makefile to pin Drupal modules at a specific version.

; drush make API version
api = 2

; Drupal core
core = 7.x

;****************************************
; Modules
;****************************************


; Use 3.x branch of jquery since we started using it earlier for some reason.
projects[jquery_update][version] = 3

; The move to 3.x requires changing jQuery versions, so pin to 2.x
projects[nice_menus][version] = 2

; The recommended release is on the 1.x branch, so we pin to the latest
; supported version on the 1.x branch
; For updates, check: https://www.drupal.org/project/i18n
projects[i18n][version] = 1

; Google Recaptcha has recommended all users upgrade to latest version
projects[recaptcha][version] = 2

; superfish 2 need an update to the superfish lib
; https://www.drupal.org/node/2622774
; we stick with the 1.x branch for now
; rdm#40559 update: superfish disappeared from drupal build so we hardcode the download link
projects[superfish][download][type] = get
projects[superfish][download][url] = https://www.drupal.org/files/projects/superfish-7.x-1.9.zip

; lightbox2 was previously in the 1.x version and 2 is a major update.
; will stick to version 1.x for now - Emmanuel 2016-04-26
projects[lightbox2] = 1

; ckeditor is EOL and has no stable release anymore
; https://www.drupal.org/project/ckeditor
; We'll stick with ckeditor-7.x-1.23 for now
; See: rm#46400
projects[ckeditor][download][type] = get
projects[ckeditor][download][url] = https://ftp.drupal.org/files/projects/ckeditor-7.x-1.23.zip

; views_slideshow is EOL and has no stable for Drupal 7 anymore
; https://www.drupal.org/project/views_slideshow
; We pin the version currently in the lockfiles, 3.10
; See rm 47789
projects[views_slideshow][download][type] = get
projects[views_slideshow][download][url] = https://ftp.drupal.org/files/projects/views_slideshow-7.x-3.10.zip

;****************************************
; End
;****************************************
