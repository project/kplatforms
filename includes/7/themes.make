;****************************************
; General
;****************************************

; Description
; A drush makefile for Drupal themes.

; drush make API version
api = 2

; Drupal core
core = 7.x

;****************************************
; Themes
;****************************************

projects[adaptivetheme][type] = theme
projects[adminimal_theme][type] = theme
projects[zen][type] = theme
projects[zen][version] = 5.6

;****************************************
; End
;****************************************
