KPLATFORMS - DRUPAL 9
=====================

## Introduction

To build the Kplatforms for Drupal 9, we use mainly composer and not drush make.

We still use some scripts to build the Kplatforms for Drupal 9, but this is just
to facilitate the transition toward composer and help to keep the possibility of
building the Kplatforms for Drupal 7, CiviCRM/Drupal 7 and Drupal 8.

You will find in ./include/9/ the following files:

* composer.json

We also use the file ./stubs/Drupal9.make as a place holder for Drupal 9 so we
can still use ./scripts/rebuild-kplatforms.sh to build the Kplatforms for Drupal
7, CiviCRM/Drupal 7, Drupal 8 and Drupal 9 with one script.

## How to build the Drupal 9 Kplatforms

Edit first ./include/9/composer.json to update the Drupal 9 version and to add
new libraries or modules.

Use ./scripts/rebuild-kplatforms.sh.

After the build, we generate the release notes with

`composer show > Drupal9.RELEASE_NOTES.txt`

Once the Drupal 9 build is completed, you should see something like this:

build/
| Drupal9.RELEASE_NOTES.txt
| Drupal9-/
| | vendor/
| | web/
| | composer.json
| | composer.lock
