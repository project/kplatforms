FROM docker.io/php:7.3-cli

# Package dependencies
RUN apt-get update && apt-get install -y \
        git \
        make \
        wget

RUN apt-get install -y \
        libzip4 \
        libzip-dev \
        zip \
        zlib1g-dev \
 && docker-php-ext-configure zip \
 && docker-php-ext-install zip

RUN apt-get install -y \
    libpng-dev \
 && docker-php-ext-configure gd \
 && docker-php-ext-install gd

# Install composer
RUN /usr/bin/wget -q 'https://getcomposer.org/download/2.5.1/composer.phar' -O /usr/bin/composer
RUN chmod +x /usr/bin/composer

# Create a non-root user for operations
RUN useradd -m -d /var/lib/builder builder
RUN mkdir -p /src
RUN chown builder: /src
USER builder

# Install drush and the extension make-diff
RUN composer -q global require drush/drush=8.x
RUN mkdir -p /var/lib/builder/.drush/commands
RUN /usr/bin/git clone https://github.com/PraxisLabs/make_diff.git /var/lib/builder/.drush/commands/make_diff
RUN /var/lib/builder/.composer/vendor/bin/drush cc drush

# Set up environment information
ENV DRUSH=/var/lib/builder/.composer/vendor/bin/drush
WORKDIR /src
VOLUME /src

# To create kplatforms:
#   cd /path/to/kplatforms
#   podman build -t kplatforms .
#   podman run -it -v $(pwd):/src:U kplatforms:latest /src/scripts/rebuild-kplatforms.sh
#   podman unshare chown -R 0:0 ./
